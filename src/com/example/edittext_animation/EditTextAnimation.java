package com.example.edittext_animation;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;

import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class EditTextAnimation extends Activity implements OnClickListener, OnFocusChangeListener{
	RelativeLayout rel;
	EditText edit1,edit_no;
	Button btn_submit;
ImageView img_test;
	TextView txt_name,txt_no;
@Override
protected void onCreate(Bundle savedInstance)
{
	super.onCreate(savedInstance);
setContentView(R.layout.edit_text);
rel=(RelativeLayout)findViewById(R.id.rel_edit);
edit1=(EditText)findViewById(R.id.edit);
img_test=(ImageView)findViewById(R.id.img_entry);
edit_no=(EditText)findViewById(R.id.edit_no);
txt_name=(TextView)findViewById(R.id.textView1);
txt_no=(TextView)findViewById(R.id.txt_no);
btn_submit=(Button)findViewById(R.id.btn_submit);
img_test.startAnimation(AnimationUtils.loadAnimation(EditTextAnimation.this,R.anim.rotate));
edit1.setOnClickListener(this);
edit_no.setOnClickListener(this);
/*edit_no.setOnFocusChangeListener(this);
edit1.setOnFocusChangeListener(this);*/
btn_submit.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		rel.startAnimation(AnimationUtils.loadAnimation(EditTextAnimation.this,R.anim.shake));
	}
});


}


@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	switch (v.getId()) {
	case R.id.edit:
		edit1.setHint("");
		txt_name.setText("Enter your Name");
		txt_name.startAnimation(AnimationUtils.loadAnimation(EditTextAnimation.this,android.R.anim.slide_in_left));
		break;
	case R.id.edit_no:
		edit_no.setHint("");
		txt_no.setText("Enter your Number");
		txt_no.setAnimation(AnimationUtils.loadAnimation(EditTextAnimation.this,android.R.anim.slide_in_left));
		break;
}
}


@Override
public void onFocusChange(View arg0, boolean arg1) {
	// TODO Auto-generated method stub
	switch (arg0.getId()) {
	case R.id.edit:
		edit1.setHint("");
		txt_name.setText("Enter your Name");
		break;
	case R.id.edit_no:
		edit_no.setHint("");
		txt_no.setText("Enter your Number");
		break;

	
	
	
}
}
}
