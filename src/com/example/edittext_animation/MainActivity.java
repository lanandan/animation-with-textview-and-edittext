package com.example.edittext_animation;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity implements OnClickListener{
Button btn_slideleft,btn_shake,btn_fade_in,btn_fade_out,btn_slide_up,btn_slide_down;
TextView txt_hello;
LinearLayout linear_lay;
EditText edt_test;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btn_slideleft=(Button)findViewById(R.id.btn_slideleft);
		btn_shake=(Button)findViewById(R.id.btn_shake);
		btn_slide_up=(Button)findViewById(R.id.btn_slide_up);
		btn_slide_down=(Button)findViewById(R.id.btn_slide_down);
		btn_fade_in=(Button)findViewById(R.id.btn_fade_in);
		btn_fade_out=(Button)findViewById(R.id.btn_fade_out);
		txt_hello=(TextView)findViewById(R.id.txt_helloo);
		/*linear_lay=(LinearLayout)findViewById(R.id.linear_lay1);
		edt_test=(EditText)findViewById(R.id.edt_test);*/
		btn_shake.setOnClickListener(this);
		btn_fade_out.setOnClickListener(this);
		btn_fade_in.setOnClickListener(this);
		btn_slideleft.setOnClickListener(this);
		btn_slide_down.setOnClickListener(this);
		btn_slide_up.setOnClickListener(this);
		/*txt_hello=(EditText)findViewById(R.id.txt_hello);*/
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_shake:
			
			txt_hello.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.shake));
			break;
		case R.id.btn_slideleft:
			txt_hello.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,android.R.anim.slide_in_left));
			break;
		case R.id.btn_fade_in:
			txt_hello.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.fade_in));
			break;
		case R.id.btn_fade_out:
			
			txt_hello.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.fade_out));
			break;
		case R.id.btn_slide_down:
			
			txt_hello.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.slide_down));
			break;
		case R.id.btn_slide_up:
			
			txt_hello.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.slide_up));
			break;
		}
	}
	}
