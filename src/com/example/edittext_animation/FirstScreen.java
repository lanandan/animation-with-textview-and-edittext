package com.example.edittext_animation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class FirstScreen extends Activity implements OnClickListener {
	Button btn_animate_edittext,btn_animate_textview;
@Override
protected void onCreate(Bundle savedInstanceState)
{
	super.onCreate(savedInstanceState);
	setContentView(R.layout.first_screen);
	btn_animate_edittext=(Button)findViewById(R.id.btn_animate_edittext);
	btn_animate_textview=(Button)findViewById(R.id.btn_animate_textview);
	btn_animate_edittext.setOnClickListener(this);
	btn_animate_textview.setOnClickListener(this);
	}
@Override
public void onClick(View arg0) {
	// TODO Auto-generated method stub
	Intent i=new Intent();
	switch (arg0.getId()) {
	case R.id.btn_animate_edittext:
		i.setClass(FirstScreen.this, EditTextAnimation.class);
		startActivity(i);
		break;
	case R.id.btn_animate_textview:
		i.setClass(FirstScreen.this, MainActivity.class);
		startActivity(i);
		break;
		}
	}
	}
